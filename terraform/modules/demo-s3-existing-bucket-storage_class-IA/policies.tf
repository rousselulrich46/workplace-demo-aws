# IAM Policy - to list all S3 buckets in the account - to be attached to the created user
data "aws_iam_policy_document" "s3_list" {
  statement {
    sid       = "ListS3Buckets"
    effect    = "Allow"
    actions   = ["s3:ListAllMyBuckets"]
    resources = ["*"] # List all S3 buckets in the account
  }
}

# S3 Bucket Policy -  to allow access to a particular bucket's objects - to be attached to bucket
# data "aws_iam_policy_document" "bucket_policy_document" {
#   statement {
#     principals {
#       type        = "AWS"
#       identifiers = [aws_iam_user.user.arn]
#     }
#     actions = [
#       "s3:GetObject",
#       "s3:ListBucket",
#     ]
#     resources = [
#       aws_s3_bucket.bucket.arn,
#       "${aws_s3_bucket.bucket.arn}/*",
#     ]
#   }
# }









# resource "aws_iam_policy" "s3_bucket_policy" {
#   name        = "S3BucketPolicy"
#   path        = "/"
#   description = "Policy for S3 bucket access"

#   policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Effect": "Allow",
#       "Action": [
#         "s3:GetBucketLocation",
#         "s3:ListAllMyBuckets",
#         "s3:ListBucket"
#       ],
#       "Resource": "arn:aws:s3:::*"
#     },
#     {
#       "Effect": "Allow",
#       "Action": [
#         "s3:GetObject",
#         "s3:PutObject",
#         "s3:DeleteObject"
#       ],
#       "Resource": "arn:aws:s3:::*/*"
#     }
#   ]
# }
# EOF
# }
