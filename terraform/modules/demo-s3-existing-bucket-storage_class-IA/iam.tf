# resource "aws_iam_policy" "s3_bucket_policy" {
#   name        = "S3BucketPolicy"
#   path        = "/"
#   description = "Policy for S3 bucket access"

#   policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Effect": "Allow",
#       "Action": [
#         "s3:GetBucketLocation",
#         "s3:ListAllMyBuckets",
#         "s3:ListBucket"
#       ],
#       "Resource": "arn:aws:s3:::*"
#     },
#     {
#       "Effect": "Allow",
#       "Action": [
#         "s3:GetObject",
#         "s3:PutObject",
#         "s3:DeleteObject"
#       ],
#       "Resource": "arn:aws:s3:::*/*"
#     }
#   ]
# }
# EOF
# }
