module "bucket" {
  source = "git::https://gitlab.com/rousselulrich46/workplace-terraform-aws.git?ref=feat_modify_default_storage_class"
}

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region
}

resource "aws_s3_bucket" "bucket" {
  bucket = var.bucket_name
  acl    = "private"
}

resource "aws_s3_bucket_lifecycle_configuration" "ainakamzou" {
  bucket = "ainakamzou"  # Remplacez par le nom de votre bucket existant

  rule {
    id      = "transition-rule"
    status  = "Enabled"

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    expiration {
      days = 46
    }

    abort_incomplete_multipart_upload {
      days_after_initiation = 1
    }
  }
}

# creating a S3 bucket policy
# resource "aws_s3_bucket_policy" "bucket_policy" {
#   bucket = aws_s3_bucket.bucket.id
#   policy = data.aws_iam_policy_document.bucket_policy_document.json
# }

# resource "aws_s3_bucket_policy" "bucket_policy" {
#   bucket = aws_s3_bucket.bucket.id
#   policy = jsonencode({
#     "Version": "2012-10-17",
#     "Statement": [
#       {
#         "Sid": "AllowPublicRead",
#         "Effect": "Allow",
#         "Principal": "*",
#         "Action": [
#           "s3:GetObject"
#         ],
#         "Resource": [
#           "${aws_s3_bucket.bucket.arn}/*"
#         ]
#       }
#     ]
#   })
# }
# S3 Bucket Policy -  to allow access to a particular bucket's objects - to be attached to bucket
# data "aws_iam_policy_document" "bucket_policy_document" {
#   statement {
#     principals {
#       type        = "AWS"
#       identifiers = [aws_iam_user.user.arn]
#     }
#     actions = [
#       "s3:GetObject",
#       "s3:ListBucket",
#     ]
#     resources = [
#       aws_s3_bucket.bucket.arn,
#       "${aws_s3_bucket.bucket.arn}/*",
#     ]
#   }
# }


data "aws_iam_policy_document" "bucket_policy_document" {
  statement {
    actions = [
      "s3:GetObject",
      "s3:PutObject",
      "s3:ListBucket",
    ]

    resources = [
      aws_s3_bucket.bucket.arn,
      "${aws_s3_bucket.bucket.arn}/*",
    ]
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "example_lifecycle" {
  bucket = "example-bucket"  # Remplacez par le nom de votre bucket existant

  rule {
    id      = "transition-rule"
    status  = "Enabled"

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    expiration {
      days = 45
    }

    abort_incomplete_multipart_upload {
      days_after_initiation = 1
    }
  }
}






# resource "aws_s3_bucket" "this" {
#   bucket = local.bucket_name
#   tags   = merge(var.tags, { Name = local.bucket_name })

#   dynamic "lifecycle_rule" {
#     for_each = var.default_storage_class == "STANDARD" ? [1] : []

#     content {
#       id      = "change-to-IA-after-60days"
#       enabled = true

#       transition {
#         days          = 30
#         storage_class = "STANDARD_IA"
#       }

#       expiration {
#         days = 35
#       }

#       abort_incomplete_multipart_upload_days = 1
#     }
#   }
# }


