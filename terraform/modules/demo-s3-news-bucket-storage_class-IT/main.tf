module "bucket" {
  source = "git::https://gitlab.com/rousselulrich46/workplace-terraform-aws.git?ref=feat_modify_default_storage_class"
}

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region
}


resource "aws_s3_bucket_lifecycle_configuration" "lifecycle_config" {
  bucket = aws_s3_bucket.bucket.id

  rule {
    id      = "DefaultToIntelligentTiering"
    status  = "Enabled"

    transition {
      days          = 0
      storage_class = "INTELLIGENT_TIERING"
    }

    expiration {
      days = 15
    }

    abort_incomplete_multipart_upload {
      days_after_initiation = 1
    }
  }
}

resource "aws_s3_bucket" "bucket" {
  bucket = local.bucket_name
  tags   = merge(var.tags, { Name = local.bucket_name })
}
